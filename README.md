# 项目介绍
当前项目基于python 3.10.13

定时关机项目：
1. 指定多少小时后关机
2. 在指定时间关机


# 依赖环境
```shell
pip install -r requirements.txt
```

打包单个文件
```shell
pyinstaller single-main.spec -y
```
```shell
single-main.bat
```
打包多个文件
```shell
pyinstaller more-main.spec -y
```
```shell
more-main.bat
```